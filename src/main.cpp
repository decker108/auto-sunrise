
#include <Arduino.h>
#include <Stepper.h>

#define STEPS_PER_MOTOR_REVOLUTION 32
#define STEPS_PER_OUTPUT_REVOLUTION 32 * 64  //2048

Stepper small_stepper(STEPS_PER_MOTOR_REVOLUTION, 6, 8, 7, 9);

int  Steps2Take;

void setup() {
  Serial.begin(9600);
  Serial.println("Ready");
}

void loop() {
  if (Serial.available()) {
    char ch = Serial.read();
    switch(ch) {
      case 'w':
        Steps2Take  =  STEPS_PER_OUTPUT_REVOLUTION ;  // Rotate CW 1 turn
        small_stepper.setSpeed(500);
        small_stepper.step(Steps2Take);
        delay(100);
        break;
      case 'q':
        Steps2Take  =  - STEPS_PER_OUTPUT_REVOLUTION;  // Rotate CCW 1 turn
        small_stepper.setSpeed(500);  // 700 a good max speed??
        small_stepper.step(Steps2Take);
        delay(100);
        break;
    }
  }
}
